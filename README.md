PROGRAMMING ASSIGNMENT
Introductions

To recover the original signal from a distorted signal using two methods and comparing them to the actual signal to determine the better method that should be implemented. Method one : Denoising followed by Deblurring , Method two: Deblurring followed by Denoising.

Requirements

- modules and libraries used: numpy, math, matplotlib.pyplot
- data.csv file should also be downloaded
 


Installation

- steps to download the modules or how to use it
-numpy
  -pip install numpy or
  -python -m pip install numpy

-pandas
  -pip install pandas

-matplotlib.pyplot 
  -pip install matplotlib

Steps to run the code
  - You need to keep data.csv and python file in same folder
  - then you can open the folder in vscode
  - lastly you need to run the sns_main.py file
  - on running sns_main.py file graphs would start showing up on after another i.e once you close one image the second would appear
    the order of images is:

	1. input , output signal
	2. denoised signal
	3. deblurred signal
	4. x1 and x comparison
	5. x2 and x comparison
