
# required imports

import pandas as pd  # for reading csv file
import numpy as np  # for math components
import matplotlib.pyplot as plt  # to plot graphs


# read data from csv file

def dataRead():
    dataFeed = pd.read_csv('data.csv', sep=',')
    return dataFeed


data = dataRead()
x = data['x[n]']  # stores x[n] values in x
y = data['y[n]']  # stores y[n] values in y


# plot of input and output signals

def signalPlot(a, b):
    t = np.arange(0, 193, 1)
    plt.plot(t, a, color='k',
             linewidth=1, label='Clean signal x[n]')
    plt.plot(t, b, color='c', linewidth=1.5, label='Distorted Signal y[n]')
    plt.title('input and output signals')
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.show()


signalPlot(x, y)


# function for denoising the signal in n domain

def denoise_signal(signal):

    N = len(signal)
    denoised = []

    # for n=0

    l = (y[0] + y[0] + y[1]) / 3
    denoised.append(l)

    # for n=1

    l1 = (y[0] + y[1] + y[2])/3
    denoised.append(l1)

    # for signals in between

    for i in range(2, N-2):
        k = (y[i-2]+y[i-1] + y[i] + y[i+1] + y[i+2]) / 5
        denoised.append(k)

    # for n=191

    m1 = (y[190] + y[191] + y[192]) / 3
    denoised.append(m1)

    # for n=192

    m = (y[191] + y[192] + y[192]) / 3
    denoised.append(m)

    return denoised


y_denoised = denoise_signal(y)


# plot of only denoised signal

def signalPlot(a, b):
    t = np.arange(0, 193, 1)
    plt.plot(t, a, color='k',
             linewidth=1, label='x[n]')
    plt.plot(t, b, color='g',
             linewidth=1.5, label='Denoised signal')
    plt.title('denoised signal')
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.show()


signalPlot(x, y_denoised)


# for deblur system
# finding H[jw] i.e discrete fourier transform of impulse response

t = np.linspace(-np.pi, np.pi, 193)
h_transform = np.cos(2*t)/8 + np.cos(t)/2 + 3/8

h_list = h_transform.tolist()
h_list[0] = h_list[1]
h_list[192] = h_list[191]


# for deblur system
# getting fourier transform of signal

def FourierOut(signal):
    d = []
    s = signal
    for i in range(193):
        X = 0
        for k in range(193):
            t = s[k]
            X += t*np.exp(-2j * np.pi * i * k / 193)
        d.append((X))
    return d


y_transform = FourierOut(y)
x_transform = FourierOut(x)


# for deblur system
# function to find deblurred signal of trnasformed signal
# it gives output directly in n domain while taked input in frequency domain

def deblur(transformed_signal):
    # deblur signal using convolution property
    w = []
    for i in range(193):
        if (h_list[i] > 0.01):
            w.append(transformed_signal[i]/(h_list[i]*1000))
        else:
            w.append(transformed_signal[i])
    # inverse DTFT of w
    inverse = []
    w_out = []
    for i in range(193):
        X = 0
        for k in range(193):
            t = w[k]
            X += t*np.exp(2j * np.pi * i * k / 193)
        inverse.append(X/193)

    # to get absolute value of complex value
    for i in inverse:
        w_out.append(abs(i))
    return w_out


w_inverse = deblur(y_transform)


# plot of only deblurred signal
def signalPlot(a, b):
    t = np.arange(0, 193, 1)
    plt.plot(t, a, color='k',
             linewidth=1, label='x[n]')
    plt.plot(t, b, color='r',
             linewidth=1.5, label='Deblur')
    plt.title('deblurred signal')
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.show()


signalPlot(x, w_inverse)


# a) denoising and then deblur

denoised_signal = denoise_signal(y)
transform = FourierOut(denoised_signal)
x1 = deblur(transform)


# plot comparing x1 and x

def signalPlot(a, b):
    t = np.arange(0, 193, 1)
    plt.plot(t, a, color='k',
             linewidth=1, label='x[n]')
    plt.plot(t, b, color='m',
             linewidth=1.5, label='x1[n]')
    plt.title('comparing x1 and x')
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.show()


signalPlot(x, x1)


# b) deblurring then denoising

y_transform = FourierOut(y)
deblurred = deblur(y_transform)
x2 = denoise_signal(deblurred)


# comparing x2 and x plots

def signalPlot(a, b):
    t = np.arange(0, 193, 1)
    plt.plot(t, a, color='k',
             linewidth=1, label='x[n]')
    plt.plot(t, b, color='c',
             linewidth=1.5, label='x2[n]')
    plt.title('comparing x2 and x plots')
    plt.xlim(t[0], t[-1])
    plt.legend()
    plt.show()


signalPlot(x, x2)
